package UM.FDS.tp3_2;
import tp3_2.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
	Courriel c1 = new Courriel("téji.savaniermhsc.mtp","nomination ballon d'or 2k21","vous etes ballon d'or 2k21 PJ : le boss","/src/tj.png");
    Courriel c2 = new Courriel();
    Courriel c3 = new Courriel();
	@Test
    public void test_verif_email_true() {
		c2.setAdresse_destination("téji.savanier@mhsc.mtp");
    	assertTrue(c2.verif_address());
    	
    }
	@Test
	public void test_verif_email_false() {
		c2.setAdresse_destination("téji.savaniermhsc.mtp");
		assertFalse(c2.verif_address());
    	
    }
    @Test
    public void test_presence_titre_true() {
    	c2.setTitre_mail("Nomination Ballon d'or");
    	assertTrue(c1.presence_titre());
    }
    @Test
    public void test_presence_titre_false() {
    	c2.setTitre_mail("");
    	assertFalse(c2.presence_titre());
    }
    @Test
    public void test_presence_PJ_true(){
    	c2.setMessage("pièce jointe : le boss");
    	c2.setPj("/src/tj.png");
    	assertTrue(c2.presence_PJ());	
    	
    }
    @Test
    public void test_presence_PJ_false(){
    	c2.setPj("");
    	assertFalse(c2.presence_PJ());	
    	
    }
   
    
    @ParameterizedTest
    @ValueSource(strings = {"oezrptroe.fk@fh.fh", "téji.savanier@mhsc.mtp"})
    public void test_true_email(String address) 
    {	c3.setAdresse_destination(address);
    	assertTrue(c3.verif_address());
    }
    @ParameterizedTest
    @ValueSource(strings = {"oezrptroefkh.fh", "téjsavanier@mhsctp"})
    public void test_false_email(String address) 
    {	c3.setAdresse_destination(address);
    	assertFalse(c3.verif_address());
    }
    
    @ParameterizedTest(name="Test sur la forme de l'adresse email")
    @MethodSource("tabRes")
    public void test_true_email(String adress, String raison) 
    {	
    	ArrayList<String> verif_list = new ArrayList<String>();
    	verif_list.add(adress);
    	verif_list.add(raison);
    	c3.setAdresse_destination(adress);
    	ArrayList<String> list = c3.verif_adresse2();
    	assertEquals(list, verif_list);
    }
    	
    private static Stream<Arguments> tabRes()
    	{
    		return Stream.of(
    				Arguments.of("téji.savaniermhsc.mtp", "@ manquant"),
    				Arguments.of("téjisavaniermhsc.mtp", "Adresse email totalement incorrecte"),
    				Arguments.of("téjisavanier@mhsc.mtp", "Points manquant"));
    	}
}

  
    
    	
    

    	
    
    
    