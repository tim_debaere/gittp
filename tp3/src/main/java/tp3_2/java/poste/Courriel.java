package tp3_2;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Courriel {
	
	//Attributs
	
	private String adresse_destination;
	private String titre_mail;
	private String message;
	private String pj;
	
	//Constructeurs
	public Courriel()
	{
		this.adresse_destination="";
		this.titre_mail="";
		this.message="";
		this.pj="";
	}
	public Courriel(String adresse_destination, String titre_mail, String message, String pj) {
		super();
		this.adresse_destination = adresse_destination;
		this.titre_mail = titre_mail;
		this.message = message;
		this.pj = pj;
	}
	
	//Accesseurs
	
	public String getAdresse_destination() {
		return adresse_destination;
	}
	public void setAdresse_destination(String adresse_destination) {
		this.adresse_destination = adresse_destination;
	}
	public String getTitre_mail() {
		return titre_mail;
	}
	public void setTitre_mail(String titre_mail) {
		this.titre_mail = titre_mail;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPj() {
		return pj;
	}
	public void setPj(String pj) {
		this.pj = pj;
	}
	
	//Méthodes
	
	public boolean verif_address() {
    	
    	String exp = "^.+\\..+@.+\\..+$";
    	return (this.getAdresse_destination()).matches(exp);
    	
    	
    }
	public boolean presence_titre()
	{
		return (this.getTitre_mail().length()>0);
	}
	public boolean presence_PJ()
	{
		return (this.getMessage().matches(".*(PJ|jointe|joint).*") && this.getPj().length()>0);
	}
	
	public ArrayList<String> verif_adresse2()
	{
		String adresse = this.getAdresse_destination();
		ArrayList<String> res = new ArrayList<String>(); 	
		String exp = "^.+\\..+@.+\\..+$";
		if(adresse.matches(exp))
			return res;	
		else
		{
			res.add(adresse);
			if(adresse.matches("^.+\\..+\\..+$"))
				res.add("@ manquant");
			
			else if(adresse.matches("^.+.+@.+.+$"))
				res.add("Points manquant");
			else
				res.add("Adresse email totalement incorrecte");
		}
		return res;
	}
}
