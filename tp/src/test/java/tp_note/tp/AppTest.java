package tp_note.tp;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    
    @Test
    public void affectationCorrecte()
    {
    	Sujet s = new Sujet("Un sujet", /*les parametres*/);
    	Groupe g = new Groupe(/*les parametres*/);
    	g.affecter(s);
    	assertSame(g.sujet, s);
    	assertSame(s.groupe, g);	
    }
}
